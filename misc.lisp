
(in-package #:misc)

(defmethod standard-str ((s string))
  (concatenate 'string
               (loop for c across s
                     when (and (< 31 (char-code c))
                               (> #xfe00 (char-code c)))
                     collect c)))

(defun fold-fns (fns val)
  (reduce #'(lambda (f s) (funcall f s))
          fns :from-end t :initial-value val))

(defun make-upcase-keyword (name)
  (values (intern (string-upcase name) "KEYWORD")))

;; credits: Paul Graham, On Lisp

(defmacro acond (&rest clauses)
  (if (null clauses)
      nil
      (let ((cl1 (car clauses))
            (sym (gensym)))
        `(let ((,sym ,(car cl1)))
           (if ,sym
               (let ((it ,sym)) ,@(cdr cl1))
               (acond ,@(cdr clauses)))))))

(defmacro aif (test-form then-form &optional else-form)
  `(let ((it ,test-form)) (if it ,then-form ,else-form)))

(defmacro aand (&rest args)
  "Hint: (aand (owner x) (address it) (town it))"
  (cond ((null args) t)
        ((null (cdr args)) (car args))
        (t `(aif ,(car args) (aand ,@(cdr args))))))

(defmacro awhen (test-form &body body)
  `(aif ,test-form (progn ,@body)))

(defmacro awhile (expr &body body)
  `(do ((it ,expr ,expr))
       ((not it))
     ,@body))

(defmacro until (test &rest body) `(do () (,test) ,@body))

(defmacro while (test &rest body) `(do () ((not ,test)) ,@body))

(defun mkstr (&rest args)
  (with-output-to-string (s)
    (dolist (a args) (princ a s))))

(defun symb (&rest args)
  (values (intern (apply #'mkstr args))))

(defun reread (&rest args)
  "Intern symbol in package with '(reread 'package \"::\" 'symbol)'"
  (values (read-from-string (apply #'mkstr args))))

;; https://lispblog.xach.com/post/147048601608/querying-plists

(defun %compile-plist-query (query)
  (labels ((callfun (object)
             (lambda (fun)
               (funcall fun object)))
           (compile-= (keyword value)
             (lambda (plist)
               (equal (getf plist keyword) value)))
           (compile-and (funs)
             (lambda (plist)
               (every (callfun plist) funs)))
           (compile-or (funs)
             (lambda (plist)
               (some (callfun plist) funs)))
           (compile-not (fun)
             (lambda (plist)
               (not (funcall fun plist)))))
    (let ((operator (first query))
          (operands (rest query)))
      (ecase operator
        (:=
         (compile-= (first operands) (second operands)))
        (:and
         (compile-and (mapcar #'%compile-plist-query operands)))
        (:or
         (compile-or (mapcar #'%compile-plist-query operands)))
        (:not
         (compile-not (%compile-plist-query (first operands))))))))

(defun query-plists (query plists)
  (remove-if-not (%compile-plist-query query) plists))

;; ---

(defun write-string-to-file (name content)
    (with-open-file (stream name
        :direction :output
        :if-exists :overwrite
        :if-does-not-exist :create)
    (format stream content)))


