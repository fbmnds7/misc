;;;; package.lisp

(defpackage #:misc
  (:use #:cl)
  (:export #:aand
           #:acond
           #:aif
           #:awhen
           #:awhile
           #:fold-fns
           #:make-upcase-keyword
           #:mkstr
           #:query-plists
           #:reread
           #:standard-str
           #:sha256-file
           #:symb
           #:until
           #:while
           #:write-string-to-file))

