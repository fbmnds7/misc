
(defpackage #:libsha256
  (:use #:cl)
  #-ccl (:local-nicknames (#:cffi #:cffi))
  (:export sha256-file))

(in-package #:libsha256)

(cffi:define-foreign-library libsha256
    (:unix "libsha256.so")
  (t (:default "libsha256")))

(cffi:define-foreign-library libssl
    (:unix (:or "libssl.so.1.0.0" "libssl.so"))
  (t (:default "libssl")))

(cffi:use-foreign-library libssl)
(cffi:use-foreign-library libsha256)

(cffi:defcfun "sha256_file" :void (path :pointer) (hash :string))

(in-package #:misc)

(defun sha256-file (path)
  (let ((hash (cffi:foreign-string-alloc (make-string 64))))
    (cffi:with-foreign-string (s path)
      (libsha256:sha256-file s hash)
      (dotimes (i 64) (format t "~A"
                              (code-char (cffi:mem-aref hash :char i)))))))

